//
//  Task.swift
//  task
//
//  Created by MAC on 23/4/22.
//

import Foundation


struct Task: Codable{
    
    let id : Int
    let title : String
    let completed : Bool
}

