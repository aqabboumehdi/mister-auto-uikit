//
//  User.swift
//  task
//
//  Created by MAC on 23/4/22.
//

import Foundation

struct User: Codable{
    
    let id : Int
    let name : String
    let username : String
    let email : String
    let phone : String
    
}

