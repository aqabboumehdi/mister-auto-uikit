//
//  AppFunctions.swift
//  task
//
//  Created by MAC on 23/4/22.
//

import Foundation


class AppFunctions{
    
    static func getUrlWithParams(url: String, parameters: [String: String]?) -> String {
        var urlComps = URLComponents(string: url)!
        if(parameters != nil) {
            var queryItems = [URLQueryItem]()
            for (param, value) in parameters! {
                let queryItem = URLQueryItem(name: param, value: value)
                queryItems.append(queryItem)
            }
            urlComps.queryItems = queryItems
        }
        let stringUrl: String = urlComps.url!.absoluteString
        return String(stringUrl)
    }
}
