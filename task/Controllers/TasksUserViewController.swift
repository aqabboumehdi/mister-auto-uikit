//
//  TasksUserViewController.swift
//  task
//
//  Created by MAC on 23/4/22.
//

import UIKit

class TasksUserViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {

    var currentUserId : Int = -1
    var currentUserName : String = ""
    var currentUserEmail : String = ""
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var taskTableView: UITableView!
    
    
    var tasks : [Task] = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        taskTableView.delegate = self
        taskTableView.dataSource = self
        // Do any additional setup after loading the view.
        nameLabel.text = currentUserName
        emailLabel.text = currentUserEmail
        
        let params = ["userId": String(currentUserId)]
        ApiService.shared.getTasksById(params: params) { tasks in
            self.tasks = tasks
            self.taskTableView.reloadData()
        } onFailed: { errorMessage in
            
        }

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count // your number of cells here
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskTableViewCell

        // Configure the cell...
        let task = tasks[indexPath.row]
        cell.titleLabel?.text = task.title
        
        cell.statusView.layer.cornerRadius = 25
        
        if(task.completed){
            
            cell.statusView.backgroundColor = UIColor(named: "completedColor")
        }else{
            cell.statusView.backgroundColor = UIColor(named: "unCompletedColor")
        }

        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
