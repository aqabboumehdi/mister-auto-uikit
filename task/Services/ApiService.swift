//
//  ApiService.swift
//  task
//
//  Created by MAC on 23/4/22.
//

import Foundation
import Alamofire


let baseUrl = "https://jsonplaceholder.typicode.com/"

enum Endpoint: String {
    
    case users = "users" // Get
    case tasks = "todos" // Get with params userId
}

class ApiService {
    
    static let shared = ApiService()
    
    var manager: Session!
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        //configuration.httpAdditionalHeaders = Session.default
        manager = Alamofire.Session(configuration: configuration)
    }
    
    func getAllUsers(onSuccess: @escaping ([User]) -> Void, onFailed : @escaping (String) -> Void) {
        
        let url =  Endpoint.users.rawValue.toApiUrl()
        
        manager.request(url).response {
            response in
            guard let data = response.data else { return }
            do {
                let decoder = JSONDecoder()
                let users = try decoder.decode([User].self, from: data)
                onSuccess(users)
            } catch let error {
                print(error)
                onFailed("")
            }
        }
    }
    
    func getTasksById(params : [String : String], onSuccess: @escaping ([Task]) -> Void, onFailed : @escaping (String) -> Void) {
        
        let url =  Endpoint.tasks.rawValue.toApiUrl()
        
        manager.request(AppFunctions.getUrlWithParams(url: url, parameters: params)).response {
            response in
            guard let data = response.data else { return }
            do {
                let decoder = JSONDecoder()
                let tasks = try decoder.decode([Task].self, from: data)
                onSuccess(tasks)
            } catch let error {
                print(error)
                onFailed("")
            }
        }
    }
    
    
}
